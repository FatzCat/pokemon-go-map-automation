# README #

This README will help you to get "Pokemon Go Map Automation" up and running.

### What's wrong with you, this can be done nice and faster? ###
* I'm far away from being an advanced scripting-guy or developer
* Just my try to get this thing to work, so be friendly and if there is a better solution, let me know

### What is this repository for? ###

* Recycles banned user accounts with fresh ones from a csv-file (Google Spreadsheet)
* Stops workers, exchanges accounts and starts workers again automatically 

### Requirements ###

* The script makes use of the gdrive binary by prasmussen - you need to download the right version for your system
* Get gdrive here: https://github.com/prasmussen/gdrive

### How do I get set up? ###

* Download autoworker bash-script
* Get setup like the example-files

### What does the file do? ###
* autoworker - checks for possibly banned accounts, removes them from hivestart and adds fresh accounts 
* bancheck   - goes through your accounts in the csv list and checks, which account is banned or good to use - writes the state to the csv file
* examples   - have a look at these files and especially the comments, you will need these to run the autoworker correctly
